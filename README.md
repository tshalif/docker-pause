# pause
Put a docker container to sleep.

This image can be used as a placeholder - for example, when you have a docker-compose YAML
template, and you want to swap an unneeded service image with an image which will not
require any resources - such as this one.
