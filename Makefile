IMAGE_TAG = tshalif/pause
IMAGE_VERSION = 1.0

all:
	docker build -t $(IMAGE_TAG):$(IMAGE_VERSION) .
	docker tag $(IMAGE_TAG):$(IMAGE_VERSION) $(IMAGE_TAG):latest
	docker push $(IMAGE_TAG):$(IMAGE_VERSION)
	docker push $(IMAGE_TAG):latest
