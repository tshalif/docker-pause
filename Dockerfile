FROM alpine:3.14
COPY Dockerfile README.md /
ENTRYPOINT ["sleep", "infinity"]
